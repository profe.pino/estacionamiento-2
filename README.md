Una empresa de estacionamientos requiere conocer el total del tiempo 
de los vehiculos estacionados en sus dependencias para poder emitir el 
boucher de salida para su pago.

Un vehiculo tiene una patente, una marca, un modelo y pueden ser 
camioneta, moto o automovil.

El estacionamiento debe registrar el vehiculo que ingresa, la fecha 
de ingreso y hora como la salida respectiva.

El estacionamiento debe, una vez registrada la salida del vehiculo un boucher
que muestre la siguiente salida:

<hr>

BOUCHER
(Fecha - Hora)
<hr>

Patente vehiculo: <br>
Fecha Ingreso   : <br>
Hora Ingreso    :	<br>		
Hora Salida     :<br>
Total Minutos   :<br>
Total Pagar     :<br>
<hr>

El valor del minuto es $20 pesos con un tope de pago de $10.000 diario.